import { useRouter } from 'next/router'

const Post = () => {
  const router = useRouter()
  const { testing, test } = router.query

  return (
    <p>
      testing: {testing} Test: {test} Query: {JSON.stringify(router.query)}
    </p>
  )
}

export default Post
