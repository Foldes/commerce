import { useRouter } from 'next/router'

const Test = () => {
  const router = useRouter()
  const { category } = router.query

  return <p>Category: {category}</p>
}

export default Test
