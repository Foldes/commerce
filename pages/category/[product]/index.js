import { useRouter } from 'next/router'

const Post = () => {
  const router = useRouter()
  const { product } = router.query

  return (
    <p>
      Product: {product} Query: {JSON.stringify(router.query)}
    </p>
  )
}

export default Post
