import { useRouter } from 'next/router'

const Post = () => {
  const router = useRouter()
  const { test, prod } = router.query

  return (
    <p>
      Post: {prod} Test: {test}
    </p>
  )
}

export default Post
