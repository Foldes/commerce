import { useRouter } from 'next/router'

const Test = () => {
  const router = useRouter()
  const { test } = router.query

  return <p>test: {test}</p>
}

export default Test
